<form>
    <div>
        <label for="radio">Radio</label>
        <input type="number" id="raadio" name="radio" value="<?= $radio ?>">
    </div>
    
    <div>
        <label for="area">Area</label>
        <input type="number" id="area" name="area" readonly="true" value="<?= $area ?>">
    </div>
    
    <div>
        <button name="calcular">Enviar</button>
    </div>       
</form>

<svg with='200' height='200'>  
    <?= circulo($radio*10) ?>
</svg>