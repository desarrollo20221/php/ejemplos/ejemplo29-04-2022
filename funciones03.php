<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $a = "Ejemplo de clase";
        // sustituir la por un -
        $final = str_replace(
                "o", // caracter a sustituir
                "-", // caracter de remplazo
                $a); // texto donde se sutituye los caracter
        echo "$final<br>"; // salida es "Ejempl- de clase"
        
        // sustituir todas las e por -
         $final1 = str_replace("e","-",$a); // salida Ej-mplo d- clas-
         $final1_1 = str_ireplace("e","-",$a); // salida -j-mplo d- clas-
         echo "$final1<br>";
         echo "$final1_1<br>";
        // sustituir clase por aula
         $final2 = str_replace("clase","aula",$a); 
         echo "$final2<br>";
        ?>
    </body>
</html>
