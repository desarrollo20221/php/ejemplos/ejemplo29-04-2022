<?php
    /**
     * funcion que recibe un array de numero
     * @param array $numeros numero a evaluar
     * @return int numero mayor
     */      
    function uno($numeros) {
        // utilizo la funcion max de php
        $numero = max($numeros);
        return $numero;
    }
    
    function unoMia($numeros=[0]) { // argumento con valor predeterminado   
        $numero = $numeros[0];
        
        // recorro el array y me quedo con el numero mayor
        foreach ($numeros as $valor){
            if($numero < $valor){
                $numero = $valor;
            }
        }
        return $numero;
        
    }
    /**
     * funcion que retorna un array ordenado de forma descendente
     * @param array $numeros vector de numeros
     * @return array array ordenado de forma descendente
     */
    function dos(&$numeros) {
        rsort($numeros);
        
    }
    
    function dosMia(&$numeros) {        
        for ($c = 0; $c < count($numeros); $c++) {
            for ($j = $c+1; $j < count($numeros); $j++) {
                if($numeros[$c]<$numeros[$j]){
                    $aux=$numeros[$c];
                    $numeros[$c]=$numeros[$j];
                    $numeros[$j]=$aux;
                }
            }
        }
    }
    
    function dosIzq(&$numeros) {
        
        do{
            $ordenado=true;
            for ($c = 0; $c < count($numeros)-1; $c++) {
                if ($numeros[$c]<$numeros[$c+1]){
                    $auxiliar= $numeros[$c];
                    $numeros[$c]=$numeros[$c+1];
                    $numeros[$c+1]=$auxiliar;
                    $ordenado=false;
                }
            }
        }while ($ordenado==false);
        
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
           echo uno([2,5,4,9,8,98]);
           
           echo unoMia([5,7,2,8,68,46]);
           
           $vector = [4,8,5,546,87,213,56]; 
           $vectorOrdenado = dos($vector);
           var_dump($vector);// el vector original ordenado
           var_dump($vectorOrdenado);// null
           
           $vectorDos=[23,34,4,67,3,2];
           dosIzq($vectorDos);
           var_dump($vectorDos);
           
           $vectorTres=[98,54,2,78,35,4,6];
           dosMia($vectorTres);
           var_dump($vectorTres);
           
        ?>
    </body>
</html>
