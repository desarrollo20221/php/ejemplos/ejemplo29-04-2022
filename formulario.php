<?php
/**
 * funcion para generar un formulario desde una plantilla
 * @param string $titulo titulo del formulario
 * @param string $etiqueta etiqueta de control
 * @param string $nombre nombre del control
 * @return string formulario con el control qenerado
 */
    function dibujar($titulo,$etiqueta,$nombre){
        // utilizamos formulario.inc como una plantilla
        // utilizaremos file_get_contents
        $formulario = file_get_contents("formulario.inc");
                
        $formulario = str_replace("{{titulo}}", $titulo, $formulario);
        $formulario = str_replace("{{etiqueta}}", $etiqueta, $formulario);
        $formulario = str_replace("{{nombre}}", $nombre, $formulario);
        
        return $formulario;
    }
    
    function dibujarArray($titulo,$etiqueta,$nombre){

        $formulario = file_get_contents("formulario.inc");
        

        $cambio = ["{{titulo}}","{{etiqueta}}","{{nombre}}"];
        $nuevo = [$titulo,$etiqueta,$nombre];
        $cambiado = str_replace($cambio, $nuevo, $formulario);
        
        return $cambiado;
        
    }
    
    
    function dibujar1($titulo,$etiqueta,$nombre) {
        include "formulario_1.inc";
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            echo dibujar("Introduccion de datos","Numero 1","numero1");
            
            echo dibujar("Introduccion de numero","Numero","numero");
            
            echo dibujar1("Introduccion de numero","Numero","numero");
            
            echo dibujarArray("Introduccion de dato","Numero 2","numero2");
        ?>
    </body>
</html>
