<?php

function calcular($radio, &$area) {
    $area = pi() * pow($radio, 2);
}

function render($radio, $area) {
    include './formulario4.php';    
}

function circulo($radio) {
    return '<circle cx="60" cy="60" r="' . $radio . '" fill="black"/>'; 
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // inicializo las variables
        $radio = 0;
        $area = 0;
        
        // controlo si he pulsado el boton
        if (isset($_GET["calcular"])) {
            $radio = $_GET["radio"];

            calcular($radio, $area);
        }
        
        // mostrar el formulario
        render($radio, $area);
        ?>
    </body>
</html>
